import React, {Component} from 'react';
import { StyleSheet, Text, View,Image} from 'react-native';

export default class Header extends Component {
  render(){
    return(
      <View style={styles.header}>
      <Image style={styles.img} source= {require('./Image/header.png')}/>
      </View>

    );
    
  }
}

const styles = StyleSheet.create({
  header: {
        width: '100%',
        height: '20%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
  },
  img: {
    width:'100%',
    height:'100%',
  }
});