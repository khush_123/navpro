import * as React from "react";
import { View, Text, TextInput, StyleSheet, TouchableOpacity, StatusBar,Image } from "react-native";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function SplashScreen({navigation}) {                                   //first screen functional component
  return (
     <View style = {styles.container}>
      
      <TouchableOpacity style ={styles.screen}
      onPress={() => navigation.navigate('Login')}>
          <Text style={styles.loginText}>LOGIN PAGE</Text>
      </TouchableOpacity>
      <TouchableOpacity style ={styles.screen}
      onPress={() => navigation.goBack()}>
          <Text style={styles.loginText}>BACK</Text>
      </TouchableOpacity>
     </View>
  );
}

export default SplashScreen ;

const styles= StyleSheet.create({                    //stylesheet for the components
    view:{
       flex: 1, 
       alignItems: 'center', 
       justifyContent: 'space-between',
    },
    container:{
       backgroundColor:'white',
       flex:1,
       alignItems:'center',
       justifyContent:"center",
    },
    img:{
      width:400,
      height:300,
    },
    img2:{
      width:300,
      height:300,
    },
    title:{
       fontWeight:'bold',
       fontSize:18, 
    },
    fast:{ 
        margin:20,
        width: 100,
        height: 100,
    },
    loginText:{
    color:"white",
    fontSize:20,
  },
    screen:{
    width:"80%",
    backgroundColor:"blue",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
   },
})