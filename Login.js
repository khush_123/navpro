import React , {Component, useState} from "react";
import { 
  View,Text,Image,Button,TextInput,
  StyleSheet,TouchableOpacity,} 
from "react-native";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Screen from "./Screen";

function Login ({navigation}) {                                                
  const [email, setEmail] = useState('');                                      
  const [password, setPassword] = useState('');

    return(
         <View style ={styles.container}>
           <TextInput style ={[styles.inputView,styles.inputText]}             
           placeholder="Username ..."
           placeholderTextColor="white"
           keyboardType="email-address"
           returnKeyType="next"
           onChangeText={email => setEmail(email)}
           defaultValue={email}
      />
           
           <TextInput style={[styles.inputView,styles.inputText]}
          placeholder='Password ...'
          placeholderTextColor='white'
          secureTextEntry
          keyboardType="number-pad"
          returnKeyType="go"
          onChangeText={password => setPassword(password)}
          defaultValue={password} 
          maxLength={4}
          />
          <TouchableOpacity>
            <Text style={styles.forgot}>Forgot Password?</Text>
          </TouchableOpacity>
          <TouchableOpacity style ={styles.loginBtn}
          onPress={() => navigation.navigate('Main Menu')}>
          <Text style={styles.loginText}>Login</Text>
          </TouchableOpacity>
          
         </View>
     );
}
export default Login;

const styles = StyleSheet.create({
   container:{
     flex:1,
     backgroundColor:"white",
     alignItems:'center',
     justifyContent:"center",
   },
   
   txt:{
     color:"orange",
     fontSize:50,
     fontStyle:'italic',
     fontWeight:'bold',
      marginBottom:40,
   },
   inputView:{
      width:"85%",
     backgroundColor:"green",
     borderRadius:25,
     height:50,
     marginBottom:20,
     justifyContent:"center",
     padding:20
  },
  inputText:{
    height:60,
    color:"white"
  },
  forgot:{
    color:"white",
    fontSize:15
  },
  loginText:{
    color:"white",
    fontSize:15
  },
   loginBtn:{
    width:"80%",
    backgroundColor:"blue",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
});