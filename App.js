import * as React from 'react';
import 'react-native-gesture-handler';
import { View,Text,Button,StyleSheet,TouchableOpacity,Image,ActivityIndicator,StatusBar} from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Screen from "./Screen";
import Login from "./Login";
import SplashScreen from "./splash";

function HomeScreen({ navigation }) {                            
  return (
    <View style={styles.view}>
      <Image 
           style = {styles.img}
           source={require("./src/Image/abc.png")}
          /> 
     <TouchableOpacity style ={styles.screen}                                       
        onPress={() => navigation.navigate('Login')}>                       
        <Text style={styles.loginText}>Next Page</Text>
    </TouchableOpacity>                      
    </View>
  );
}

const Stack = createStackNavigator();

function App() {                                           
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home Page"
      screenOptions={{
          headerStyle: {
              backgroundColor: 'white',
            },
            headerTintColor: 'white',
            headerTitleStyle: {
          fontWeight: 'bold',
        },
        }}
      >
        <Stack.Screen
              name="Start Screen" component={HomeScreen} 
                options={{
            title: 'Home Page',
       }}
       />
        <Stack.Screen name="Screen" component={SplashScreen}
         options={{
            title: 'Next Page',
          }}
         />
        <Stack.Screen name="Login" component={Login} 
          options={{
            title: 'Login Page',  
          }}
        />
        <Stack.Screen name="Main Menu" component={Screen} 
          options={{
            title: 'Home', 
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

const styles = StyleSheet.create({
    view:{
       flex: 1, 
       alignItems: 'center', 
       justifyContent: 'space-between',
    },
    container:{
       backgroundColor:'#003f5c',
       flex:1,
       alignItems:'center',
       justifyContent:"center",
    },
    img:{
      width:150,
      height:150,
      marginTop: 180,
    },
    img2:{
      width:300,
      height:300,
    },
    title:{
       fontWeight:'bold',
       fontSize:18, 
    },
    fast:{ 
        margin:20,
        width: 100,
        height: 100,
    },
    loginText:{
    color:"white",
    fontSize:20,
  },
    screen:{
    width:"80%",
    backgroundColor:"blue",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:10,
    marginBottom:100
   },
})