import * as React from "react";
import { View,Text,Button,TextInput,StyleSheet,TouchableOpacity } 
from "react-native";
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

function Screen ({navigation}) {  
        return(
            <View style = {styles.container}>
                <Text style = {styles.txt}>
                Welcome To Home</Text>
                <TouchableOpacity style ={styles.back}
                 onPress={() => navigation.popToTop()}>
                <Text style={styles.loginText}>Back</Text>
                </TouchableOpacity>       
                </View>
        );
    }

export default Screen ;

const styles= StyleSheet.create({
    container:{
     flex:1,
     backgroundColor: 'white',
     alignItems:'center',
     justifyContent:"center",
   },
   txt:{
     color:'blue',
     fontSize:40,
     fontWeight:'bold',
      marginTop:-40,
   },
   loginText:{
    color:"white",
    fontSize:20,
  },
   back:{
    width:"80%",
    backgroundColor: 'blue',
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
})